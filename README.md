# Totino's Mania mod for Sonic Mania / Plus (version 1.1-WIP)


## Installation Instructions


#### PC (Steam)

1. Download and install Mania Mod Loader. https://gamebanana.com/tools/6273

2. Copy the "TotinosManiaMod" folder into your mods folder.

3. Open the Mod Loader, check the box for the "Totino's Mania" mod, and click Save & Play.

4. Enjoy!


#### Nintendo Switch (homebrew-enabled)

1. Extract the entire contents of this archive to an empty folder.

2. Have Atmosphere CFW, SimpleModManager (https://github.com/nadrino/SimpleModManager), nxdumptool (https://github.com/DarkMatterCore/nxdumptool), and Sonic Mania *with the most recent patch* installed on your Switch. (How to install CFW and use homebrew apps is outside the scope of these instructions.)

3. Open nxdumptool.

4. Choose "dump installed SD card / eMMC content".

5. In the list, find Sonic Mania and choose that from the menu.

6. Choose "RomFS options".

7. Where it says "Use update/DLC", press right to select the option that says "(UPD)".

8. Choose "Browse RomFS section". If you did Step 7 right, there will be a line that says "Update to browse:" and not "Base application to browse:" or "DLC to browse:". Otherwise, back out and do Step 7 again.

9. Choose "Data.rsdk". This will dump the file to your SD card.

10. Access your Switch's SD card contents. This can be done most easily by turning it off and plugging your microSD card into your computer. The Data.rsdk file you dumped will be in a Sonic Mania folder located in [SD card root]/switch/nxdumptool/RomFS/.

11. Copy the Data.rsdk file to the same folder as "create Switch mod.bat" and this readme. Once you've copied it over, you can delete the file that's on your Switch.

12. Run "create Switch mod.bat".

13. Once the window closes, there should be a new folder called "mods". Copy this folder to the root of your Switch's SD card.

14. Use SimpleModManager to enable the mod.

15. Enjoy! (You can now delete the Data.rsdk file in the same folder as "create Switch mod.bat" and this readme.)


## Changelog

#### Version 1.1-WIP
- new Special Stage music
- some new and updated sound effects
- minor tweaks and fixes
- made adjustments and added install script so you can use the mod on homebrew-enabled Nintendo Switch systems
- added compatibility with the RSDKv5(U) decompilation

#### Version 1.0
- initial release


## Mod Credits

Music: SiIvaGunner (https://youtube.com/SiIvaGunner)  
Initial Lead Modding: CrossCoder (https://youtube.com/CrossCoder)  
Mod Maintenance: Harmony Friends (https://sylveon.gay/)  
Additional Graphics: Maggie, MtH, eg_9371
Additional Sound Design: MtH 

The Switch mod creator batch script utilizes the included external tools "RSDKv5Extract" and "RSDKv5Pack" versions 1.1 by MainMemory, koolkdev, Axanery, M&M, RandomTalkingBush, SuperSonic16, Rubberduckycooly, Tpot, LittlePlanetCD, Lave Slime, and breakthetargets (https://github.com/MainMemory/RSDKv5Extract).


## Homepage / Music Downloads

https://highquality.rip/totinos-mania/