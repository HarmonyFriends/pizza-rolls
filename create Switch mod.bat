@echo off
if exist Data.rsdk (
    if exist mods (
        rmdir /s /q Mods
    )
    .\_switch_installer_tools\RSDKv5Extract.exe Data.rsdk
    echo Copying mod files...
    Xcopy /e /i /y /q TotinosManiaMod\Data\ Data\Data\
    echo Packing mod...
    md "mods\Sonic Mania\Totino's Mania\contents\01009AA000FAA000\romfs\"
    .\_switch_installer_tools\RSDKv5Pack.exe Data "mods\Sonic Mania\Totino's Mania\contents\01009AA000FAA000\romfs\Data.rsdk"
    echo Cleaning up...
    rmdir /s /q Data
) else (
    echo ERROR: The Sonic Mania Data.rsdk file must be placed in the same folder as this batch file.
    echo Please refer to README.md for install instructions.
    pause
)